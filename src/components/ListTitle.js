
import React, { Component } from 'react'
import { StyleSheet,  Text, View,  } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import {
    systemWeights,
    iOSUIKit,
    sanFranciscoWeights,
    sanFranciscoSpacing,
    materialColors
} from 'react-native-typography';
import {  Icon } from 'native-base';

//import StorageSchema from "../storage/StorageSchema";

class ListTitle extends Component {

    constructor(props) {
        super(props);

        this.state = {
            
        }
    }

    componentDidMount() {
        
    }


    render() {
        const addIcon = <Icon name="ios-add-circle-outline" />
        const arrowIcon = <Icon name={!this.props.open ? 'ios-arrow-dropdown' : 'ios-arrow-dropup'} />
        return (
            <View style={styles.view}>
                <View style={{  flexDirection: 'row', justifyContent: 'space-between' }}> 
                    <Text style={styles.h2}> {this.props.text.charAt(0).toUpperCase() + this.props.text.slice(1)} </Text>
                    {this.props.addIcon ? addIcon : arrowIcon }
                </View>
                <View style={styles.underline} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    h2: {
        ...sanFranciscoWeights.heavy,
        fontSize: 26,
        letterSpacing: sanFranciscoSpacing(34),
    },
    view: {
        paddingTop: 10, marginTop: 20, paddingRight: 10, paddingBottom: 10, paddingLeft: 10
    },
    underline: {
        marginTop: 5,
        borderBottomColor: '#eff0f1',
        borderBottomWidth: 1,
    },

});
export default ListTitle