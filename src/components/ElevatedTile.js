
import React, { Component } from 'react'
import { StyleSheet, AppRegistry, Dimensions, Text, Image } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import { 
    systemWeights, 
    iOSUIKit,
    sanFranciscoWeights, 
    sanFranciscoSpacing, 
    materialColors } from 'react-native-typography';

//import RealmStorage from "../storage/StorageSchema";
import Api from "../api/API";

class ElevatedTile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            largeTileSize: 0,
            image: this.props.data.image
        }
    }

    componentDidMount() {
        console.log(this.state.image);
        
        let pageWidth = Dimensions.get('window').width;
        if (pageWidth > 600) {
            this.setState({
                largeTileSize: 350
            })
        } else {
            this.setState({
                largeTileSize: pageWidth - 30
            })
        }
    }

    largeTileSize() {
        console.log(pageWidth)
        this.setState({
            largeTileSize: pageWidth - 30
        })
    }

    onPress(){
        let ex =  Api.getExercise();

        //console.log('tets', ex)
    }

    render() {
        return (
            <ElevatedView style={[styles.tile, { 
                width: this.state.largeTileSize,
                marginRight: 10,
                marginLeft: this.props.first ? 10 : 0,
            }]}
                onPress={() => this.onPress()}
                elevation={7} elevationColor="#000" feedbackEnabled activeElevation={1} >
                
                <Image source={require("../resources/images/abs.png")} style={[styles.image, { width: this.state.largeTileSize }]}  />

                <Text style={styles.h3}> {this.props.data.title} </Text>
            </ElevatedView>
        )
    }
}
const styles = StyleSheet.create({
    h1: {
        ...sanFranciscoWeights.black,
        fontSize: 34,
        letterSpacing: sanFranciscoSpacing(34),
    },
    h2: {
        ...sanFranciscoWeights.heavy,
        fontSize: 26,
        letterSpacing: sanFranciscoSpacing(34),
    },
    h3: {
        ...sanFranciscoWeights.bold,
        fontSize: 20,
        letterSpacing: sanFranciscoSpacing(24),
        color: '#fff',
        elevation: 1,
    },
    tile: {
        position: 'relative',
        height: 250,
        backgroundColor: "transparent",
        padding: 16,
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    image: {
        flex: 1,
        height: 250,
        resizeMode: 'cover',
        borderRadius: 10,
        position: 'absolute',
    }

});
export default ElevatedTile