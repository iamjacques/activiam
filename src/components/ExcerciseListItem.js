
import React, { Component } from 'react'
import { StyleSheet, AppRegistry, TouchableOpacity, Text, Image, View } from 'react-native'
import {
    systemWeights,
    iOSUIKit,
    sanFranciscoWeights,
    sanFranciscoSpacing,
    materialColors
} from 'react-native-typography';
import { H3, Badge } from 'native-base';


class ExcerciseListItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: this.props.data
        }
    }

    componentDidMount() {

     }

    openProgramPage(){
      
    }

    render() {
        return (
            <View style={{ flex: 1, paddingLeft: 50, paddingRight: 10, marginBottom: 10 }}>
                <TouchableOpacity onPress={() => this.props.showAside()} style={[styles.box, { justifyContent: 'center', flexDirection: 'row' }]}>
                    <View style={{ backgroundColor: 'transparent', justifyContent: 'center', width: 40, height: 100 }}> 
                        <Image source={require("../resources/images/abs.png")} style={[styles.image]} />
                    </View>
                    <View style={{ backgroundColor: 'transparent', justifyContent: 'center', flex: 1, padding: 5, height: 100 }}>
                        <H3 style={styles.h3}> {this.props.data.title } </H3>
                        <View style={{ flexDirection: 'row', paddingLeft: 10, paddingTop: 3, paddingRight: 10, paddingBottom: 3}}> 
                            <Badge style={{ marginRight: 5, backgroundColor: '#cacaca' }}>
                                <Text style={{color: 'white'}} >5 Workouts</Text>
                            </Badge>
                            <Badge  style={{ marginRight: 5, backgroundColor: '#cacaca' }}>
                                <Text style={{ color: 'white' }}>50 Min</Text>
                            </Badge>
                            <Badge  style={{ marginRight: 5, backgroundColor: '#cacaca' }}>
                                <Text style={{ color: 'white' }}>120 cal</Text>
                            </Badge>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    box: {
        shadowColor: '#d9d9d9',
        borderColor: '#d9d9d930',
        borderWidth: 1,
        shadowRadius: 10,
        shadowOpacity: 0.5,
        shadowOffset: {width: 0, height: 5},
        elevation: 6,
        backgroundColor: 'white', 
        borderRadius: 10,
        height: 100
    },
    image: {
        width: 80,
        height: 80,
        resizeMode: 'cover',
        position: 'absolute',
        borderRadius: 10,
        left: -40
    
    }, 
    h1: {
        ...sanFranciscoWeights.black,
        fontSize: 34,
        letterSpacing: sanFranciscoSpacing(34),
    },
    h2: {
        ...sanFranciscoWeights.heavy,
        fontSize: 26,
        letterSpacing: sanFranciscoSpacing(34),
    },
    h3: {
        ...sanFranciscoWeights.bold,
        fontSize: 20,
        letterSpacing: sanFranciscoSpacing(24),

    },
});
export default ExcerciseListItem