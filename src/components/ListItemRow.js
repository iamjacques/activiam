
import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import {
    systemWeights,
    iOSUIKit,
    sanFranciscoWeights,
    sanFranciscoSpacing,
    materialColors
} from 'react-native-typography';
import {
    View, List, Right, Left, ListItem, Text, Body, Badge, Thumbnail
} from 'native-base';


//import StorageSchema from "../storage/StorageSchema";

class ListItemRow extends Component {

    constructor(props) {
        super(props);

        this.state = {

        }
    }

    componentDidMount() {

    }


    render() {
        return (
            <ListItem onPress={() =>  this.onPress.bind(this) } >
                <Thumbnail style={{ borderRadius: 5 }} square size={80} source={{ uri: 'https://ak4.picdn.net/shutterstock/videos/15802714/thumb/11.jpg?i10c=img.resize(height:160)' }} />
                <Body>
                    <Text>Core Strength 2</Text>
                    <Text note style={{ justifyContent: 'space-between' }} > 
                        <Badge info style={{ padding: 5 }}> <Text>5 Workout</Text> </Badge> 
                        <Badge warning> <Text>20 min</Text> </Badge> 
                        <Badge success> <Text>45 cal</Text> </Badge> 
                    </Text>
                </Body>
                <Right>
                    <Text>  </Text>
                </Right>
            </ListItem>
        )
    }
}
const styles = StyleSheet.create({
    h2: {
        ...sanFranciscoWeights.heavy,
        fontSize: 26,
        letterSpacing: sanFranciscoSpacing(34),
    },
    view: {
        paddingTop: 10, marginTop: 20, paddingRight: 10, paddingBottom: 10, paddingLeft: 10
    },
    underline: {
        marginTop: 5,
        borderBottomColor: '#eff0f1',
        borderBottomWidth: 1,
    },

});
export default ListItemRow