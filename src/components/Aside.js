
import React, { Component } from 'react'
import { StyleSheet, Text, View, Animated, TouchableOpacity } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import {
    systemWeights,
    iOSUIKit,
    sanFranciscoWeights,
    sanFranciscoSpacing,
    materialColors
} from 'react-native-typography';
import { Icon } from 'native-base';

class Aside extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    componentDidMount() {  
    }

    showAside() {
        this.props.showAside = !this.props.showAside;
        //console.log('test', this.props.showAside  )
    }

    render() {
        return (
            <View style={{ display: this.props.showAside ? 'flex' : 'none', flexDirection: 'row', position: 'absolute', top: 0, right: 0, left: 0, bottom: 0, zIndex: 10 }}>
                <View style={{ flex: 1, opacity: 0.5, backgroundColor: 'white', }}> </View>
                <View style={{ flex: 4, backgroundColor: 'white', borderLeftColor: '#d1d5da', borderLeftWidth: 1 }}>
                    {/* Content */}
                    <View style={{ position: 'absolute', bottom: 0, left: 0, top: 0, right: 0 }}>

                    </View>
                    {/* End Content */}

                    {/* Footer */}
                    <View style={{ height: 60, flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: 'transparent', position: 'absolute', bottom: 0, left: 0, right: 0 }}>
                        <TouchableOpacity onPress={() => this.showAside()} >
                            <View style={{ backgroundColor: '#bf50ff', height: 40, width: 50, borderBottomRightRadius: 50, borderTopRightRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <Icon color="#000" name="ios-arrow-round-forward"> </Icon>
                            </View>
                        </TouchableOpacity>

                        <View style={{ backgroundColor: 'green', height: 40, width: 40, borderRadius: 50, marginRight: 15, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name="ios-add-outline"> </Icon>
                        </View>

                    </View>
                    {/* End Footer */}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    h2: {
        ...sanFranciscoWeights.heavy,
        fontSize: 26,
        letterSpacing: sanFranciscoSpacing(34),
    },
    view: {
        paddingTop: 10, marginTop: 20, paddingRight: 10, paddingBottom: 10, paddingLeft: 10
    },
    underline: {
        marginTop: 5,
        borderBottomColor: '#eff0f1',
        borderBottomWidth: 1,
    },

});
export default Aside