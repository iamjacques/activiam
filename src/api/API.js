const axios = require('react-native-axios');
const endpoint = "https://wger.de/api/v2/";
const apiKey = "5ad90f60bafed92bbfac4e5105f4169d1fbfd78f";
const config = {
    headers: {
        "Authorization": `Bearer ${apiKey}`,
        "Content-Type": "application/json"
    }
};

export default Api = {
    getExercise(){
        return [{
            legs: [{
                title: 'legs Day',
                image: 'test',
                desc: 'test'
            }],
            shoulders: [{
                title: 'legs Day',
                image: 'test',
                desc: 'test'
            }],
        }];
    },

    getPrograms(){
        return {
            legsDay: [{
                title: 'legsDay',
                image: 'test',
                desc: 'test'
            }]
        };
    }
}