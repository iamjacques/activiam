export default {
    name: 'ExerciseSchema',
    primaryKey: 'id',
    properties: {
        id: {
            type: 'int'
        },
        name: {
            type: 'string'
        }
    }
}