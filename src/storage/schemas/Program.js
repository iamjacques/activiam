export default {
    name: 'ProgramSchema',
    primaryKey: 'id',
    properties: {
        id: {
            type: 'int'
        },
        name: {
            type: 'string'
        },
        programs: {
            type: 'list',
            objectType: 'ExerciseSchema',
            default: []
        }
    }
}