export default {
    name: 'UserSchema',
    primaryKey: 'id',
    properties: {
        id: {
            type: 'int'
        },
        firstName: {
            type: 'string'
        },
        lastName: {
            type: 'string'
        },
        programs: {
            type: 'list',
            objectType: 'ProgramSchema',
            default: []
        }
    }
}