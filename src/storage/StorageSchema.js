import Realm from 'realm'
import UserSchema from "./schemas/User";
import ProgramSchema from "./schemas/Program";
import ExerciseSchema from "./schemas/Exercise";



export default RealmStorage = {
    realm: new Realm({
        schema: [
            UserSchema,
            ProgramSchema,
            ExerciseSchema
        ],
    }),
    
    setUser(){
        this.realm.write(() => {
            this.realm.create('UserSchema', {
                id: 1,
                firstName: 'jacques',
                lastName: 'uwamungu',
            }, true)
        })
    },
    getUser() {
        let user = this.realm.objects('UserSchema');
        return user;
    }
}

