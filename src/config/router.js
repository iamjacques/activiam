import React from 'react';
import { Platform, Image } from 'react-native'
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon  } from 'native-base';

import MorePage from '../screens/more/morePage';
import ExercisesPage from '../screens/exercises/exercisesPage';
import ExercisePage from '../screens/exercises/exercisePage';
import ProgramsPage from '../screens/programs/programsPage';
import CurrentProgramsPage from '../screens/programs/currentProgramsPage';

export const CurrentProgramStack = StackNavigator({
    CurrentProgramsPage: {
        screen: CurrentProgramsPage,
        navigationOptions: {
            title: 'Current Program',
          },
    }, 
})

export const ExerciseStack = StackNavigator({
    ExercisePage: {
        screen: ExercisePage,
        navigationOptions: {
            title: '',
          },
    }, 
})


export const Tabs = TabNavigator({
    ProgramsPage: {
        screen: ProgramsPage,
        navigationOptions: {
            tabBarLabel: 'Programs',
            tabBarIcon: ({ tintColor }) => <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/684/684264.png'}} style={{height: 25, width: 25 }}/> 
           // tabBarIcon: ({ tintColor }) => <Icon name="ios-bookmarks-outline" size={35} color={tintColor} /> 
        },
    },
    ExercisesPage: {
        screen: ExercisesPage,
        navigationOptions: {
            tabBarLabel: 'Exercises',
            tabBarIcon: ({ tintColor }) => <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/684/684272.png' }} style={{ height: 25, width: 25 }} />
            //tabBarIcon: ({ tintColor }) => <Icon name="ios-medal-outline" size={35} color={tintColor} />
        },
    },
   
    StatsPage: {
        screen: MorePage,
        navigationOptions: {
            tabBarLabel: 'Stats',
            tabBarIcon: ({ tintColor }) => <Icon name="ios-trending-up-outline" size={35} color={tintColor} />
        },
    },
    MorePage: {
        screen: MorePage,
        navigationOptions: {
            tabBarLabel: 'More', 
            tabBarIcon: ({ tintColor }) => <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/149/149294.png' }} style={{ height: 25, width: 25 }} />
            //tabBarIcon: ({ tintColor }) => <Icon name="ios-more-outline" size={35} color={tintColor} />
        },
    }
},
{
    tabBarPosition: 'bottom',
    tabBarOptions: {
      upperCaseLabel: false,
      showIcon: true,
      style: {
          backgroundColor: 'white'
      },
      labelStyle: {
       
      },
      indicatorStyle: {
        height: 0
      },
    }
});

export const Root = StackNavigator({
    Tabs: {
        screen: Tabs
    },
    ExerciseStack: {
       screen: ExerciseStack 
    },
    ExercisePage: {
        screen: ExercisePage
    }
},
{
    mode: 'modal',
    headerMode: 'none',
})
