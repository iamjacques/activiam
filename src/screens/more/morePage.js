import React, { Component } from 'react'
import { StyleSheet, AppRegistry, Image, Dimensions, ScrollView, TouchableHighlight, Alert } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import { NavigationActions } from 'react-navigation';
import { systemWeights, iOSUIKit,sanFranciscoWeights, sanFranciscoSpacing, materialColors } from 'react-native-typography'
import { Col, Row, Grid } from 'react-native-easy-grid';
import getTheme from '../../../native-base-theme/components';
import commonColor from '../../../native-base-theme/variables/commonColor';

import {
  Container, View, Header, Separator, Content, List, Right, Left, ListItem, Text, Body, Thumbnail, Button, Icon, Title, StyleProvider,
  H1, H2,
} from 'native-base';
  


const styles = StyleSheet.create({
  h1: {
    ...sanFranciscoWeights.black,
    fontSize: 34,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h2: {
    ...sanFranciscoWeights.heavy,
    fontSize: 26,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h3: {
    ...sanFranciscoWeights.bold,
    fontSize: 20,
    letterSpacing: sanFranciscoSpacing(24),
  },
  subHeading: {
    
  },
  welcome: {
    fontSize: 20,
    color: "white",
    textAlign: "center"
  },
  instructions: {
    textAlign: "center",
    color: "white"
  }
});


export default class MorePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      largeTileSize: 0,
      smallPageTitle: '',
      largePageTitle: 'More'
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#FFF' }}>
      <StyleProvider style={getTheme(commonColor)}>
        <Header >
          <Left/>
          <Body>
            <Title>{this.state.pageTitle}</Title>
          </Body>
          <Right />
          </Header>
        </StyleProvider>  
        
        <Content>
        
          <View style={{
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: '#fff', 
          }}>
           

          
            <H1 style={ styles.h1 }> {this.state.largePageTitle}</H1>
    
            <Text style={ styles.h1 }>
             
            </Text>
          </View>



          <View style={{paddingTop: 40, marginTop: 20,  paddingRight: 10,  paddingBottom: 10,  paddingLeft: 10}}> 
            <Text style={styles.h2}> Core </Text>  
            <View
              style={{
                marginTop: 5,            
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
  }}
/>    
          </View>
          
      
          <List>
          <ListItem >
              <Thumbnail style={{borderRadius: 5}} square size={80} source={{ uri: 'https://ak4.picdn.net/shutterstock/videos/15802714/thumb/11.jpg?i10c=img.resize(height:160)' }} />
              <Body>
                <Text>Core Strength 2</Text>
                <Text note> 10 Workout | 20 min | 45 cal</Text>
              </Body>
              <Right>
                <Text>  </Text>
                </Right>
            </ListItem>

            <ListItem >
              <Thumbnail style={{borderRadius: 5}} square size={80} source={{ uri: 'https://ak8.picdn.net/shutterstock/videos/15167458/thumb/7.jpg' }} />
              <Body>
                <Text>Core Strength 2</Text>
                <Text note> 10 Workout | 20 min | 45 cal</Text>
              </Body>
              <Right>
                <Text>  </Text>
                </Right>
            </ListItem>
            </List>

            <View style={{paddingTop: 40, marginTop: 20,  paddingRight: 10,  paddingBottom: 10,  paddingLeft: 10}}> 
            <Text style={styles.h2} > Legs </Text>   
            <View
              style={{
                marginTop: 5,            
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
  }}
/>    
          </View>
          
          <List>
          <ListItem >
              <Thumbnail style={{borderRadius: 5}} square size={80} source={{ uri: 'https://cdn-mf0.heartyhosting.com/sites/mensfitness.com/files/styles/gallery_details_image/public/bentover.jpg?itok=JTmyZ6V4' }} />
              <Body>
                <Text>Beast Mode</Text>
                <Text note> 10 Workout | 20 min | 45 cal</Text>
              </Body>
              <Right>
                <Text>  </Text>
                </Right>
            </ListItem>
         
            <ListItem last>
              <Thumbnail style={{borderRadius: 5}} square size={80} source={{ uri: 'https://cdn-mf0.heartyhosting.com/sites/mensfitness.com/files/styles/gallery_slideshow_image/public/_main_jumprope_1.jpg?itok=1XQCqKKb' }} />
              <Body>
                <Text>Cardio</Text>
                <Text note> 10 Workout | 20 min | 45 cal</Text>
              </Body>
              <Right>
                <Text>  </Text>
                </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

AppRegistry.registerComponent('MorePage', () => MorePage);
