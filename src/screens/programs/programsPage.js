import React, { Component } from 'react'
import { StyleSheet, AppRegistry, Image, TouchableOpacity, Dimensions, ScrollView, TouchableHighlight, Alert } from 'react-native'
import {
  Container, View, Header, Separator, Content, ListItem, List, Right, Left, Text, Body, Thumbnail, Button, Icon, Title, StyleProvider,
  H1, H2,
} from 'native-base';
import ElevatedView from "fiber-react-native-elevated-view";
import { NavigationActions } from 'react-navigation';
import { systemWeights, iOSUIKit,sanFranciscoWeights, sanFranciscoSpacing, materialColors } from 'react-native-typography'
import { Col, Row, Grid } from 'react-native-easy-grid';
import getTheme from '../../../native-base-theme/components';
import commonColor from '../../../native-base-theme/variables/commonColor';
import  ElevatedTile  from "../../components/ElevatedTile";
import  ListTitle  from "../../components/ListTitle";
import ListItemRow from "../../components/ListItemRow";
import ExcerciseListItem from "../../components/ExcerciseListItem";
import  Api  from "../../api/API";




export default class ProgramsPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      largeTileSize: 0,
      smallPageTitle: '',
      largePageTitle: 'Programs',
      date: '',
      myPrograms: [
        {
          title: 'Back Blaster',
          image: '../resources/images/abs.png',
          desc: 'test'
        },
        {
          title: 'Legs Day',
          image: '../resources/images/abs.png',
          desc: 'test'
        },
        {
          title: 'Chest Day',
          image: '../resources/images/abs.png',
          desc: 'test'
        }
      ],
      programs: {
        absAndCore: [
          {
            title: 'Flat Bench Leg Raises',
            image: '../resources/images/abs.png',
            desc: 'This is an exercise for lower abdominal strengthening.',
            link: 'http://db.everkinetic.com/exercise/flat-bench-leg-raises/'
          },
        
          {
            title: 'Crunches',
            image: '../resources/images/abs.png',
            desc: 'This is the most common abdominal exercise and possibly the most often improperly performed. Here is how to perform it correctly.',
            link: 'http://db.everkinetic.com/exercise/crunches/'
          },
        ],
        arms: [
          {
            title: 'Tate Press',
            image: '../resources/images/abs.png',
            desc: 'This is an advanced triceps exercise which moves the muscle differently than other exercises.',
            link: 'http://db.everkinetic.com/exercise/tate-press/'
          },
        ]
      }
    }
  }




  componentDidMount() {
    let now = new Date();
    let options = {  
      weekday: 'long',
      day: 'numeric',
        year: 'numeric',
        month: 'long',
    };

    this.setState({
     date: now.toLocaleString('en-gb', options)
   }) 
    this.largeTileSize();
  }

  goToCurrentProgram() {
    const { navigate } = this.props.navigation;
      navigate('CurrentProgramsPage', { })
  }

  goToSingleExercise() {
    const { navigate } = this.props.navigation;
      navigate('ExercisePage', { })
  }

  largeTileSize() {
    let pageWidth = Dimensions.get('window').width;
    
    if (pageWidth > 600) {
      this.setState({
        largeTileSize: 350
      })
    } else  {
      this.setState({
        largeTileSize: pageWidth - 30
      })
    } 
  }

  show(cat) {
    switch (cat) {
      case 'abs':
        this.setState({
          showAbs: !this.state.showAbs
        })
        break;
      case 'arms':
        this.setState({
          showArms: !this.state.showArms
        })
        break;
      case 'back':
        this.setState({
          showBack: !this.state.showBack
        })
        break;
      case 'chest':
        this.setState({
          showChest: !this.state.showChest
        })
        break;
      case 'shoulders':
        this.setState({
          showShoulders: !this.state.showShoulders
        })
        break;
      case 'legs':
        this.setState({
          showLegs: !this.state.showLegs
        })
        break;
      case 'cardio':
        this.setState({
          showCardio: !this.state.showCardio
        })
        break;
      case 'strength':
        this.setState({
          showStrength: !this.state.showStrength
        })
        break;
    }

    console.log(cat)
  }
 

  render() {
    return (
      <Container style={{ backgroundColor: '#FFF' }}>
      <StyleProvider style={getTheme(commonColor)}>
        <Header >
          <Left/>
          <Body>
            <Title>{this.state.pageTitle}</Title>
          </Body>
          <Right />
          </Header>
        </StyleProvider>  
        
        <Content>
        
          <View style={{
            paddingLeft: 5,
            paddingRight: 5,
            backgroundColor: '#fff', 
          }}>
            <Text style={[styles.subHeading, {padding: 5} ]} > {this.state.date} </Text>
            <H1 style={ styles.h1 }> {this.state.largePageTitle}</H1>
    
            <Text style={ styles.h1 }> </Text>
          </View>

         

          <ListTitle text="My Programs" addIcon={true} />

          <ScrollView 
            showsHorizontalScrollIndicator={false}  
            directionalLockEnabled={false}
            horizontal={true}>
      
            {this.state.myPrograms.map((data, key) => {
              return (
                <ElevatedTile data={data} key={key} first={ key === 0 ? true : false} last={this.state.programs.length == key+1 ? true: false}  /> 
              );
          })}
          </ScrollView>

          <TouchableOpacity onPress={() => this.show('abs')} >
            <ListTitle text="Abs & Core" open={this.state.showAbs} />
          </TouchableOpacity>

          <View style={{ height: this.state.showAbs ? 'auto' : 0, opacity: this.state.showAbs ? 1 : 0 }}>
            {this.state.programs.absAndCore.map((data, key) => {
              return (
                <ExcerciseListItem data={data} key={key} />
              );
            })}
          </View>



          <TouchableOpacity onPress={() => this.show('arms')} >
            <ListTitle text="Arms" open={this.state.showArms} />
          </TouchableOpacity>

          <View style={{ height: this.state.showArms ? 'auto' : 0, opacity: this.state.showArms ? 1 : 0 }}>
            {this.state.programs.arms.map((data, key) => {
              return (
                <ExcerciseListItem data={data} key={key} />
              );
            })}
          </View>

        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  h1: {
    ...sanFranciscoWeights.black,
    fontSize: 34,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h2: {
    ...sanFranciscoWeights.heavy,
    fontSize: 26,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h3: {
    ...sanFranciscoWeights.bold,
    fontSize: 20,
    letterSpacing: sanFranciscoSpacing(24),
    color: '#fff',
    elevation: 1,
  },
  subHeading: {

  },
  underline: {
    marginTop: 5,
    borderBottomColor: '#eff0f1',
    borderBottomWidth: 1,
  },
  instructions: {
    textAlign: "center",
    color: "white"
  }
});

AppRegistry.registerComponent('ProgramsPage', () => ProgramsPage);