import React, { Component } from 'react'
import { StyleSheet, AppRegistry, TouchableOpacity, Image, Dimensions, ScrollView, TouchableHighlight, Alert } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import { NavigationActions } from 'react-navigation';
import { systemWeights, iOSUIKit,sanFranciscoWeights, sanFranciscoSpacing, materialColors } from 'react-native-typography'
import { Col, Row, Grid } from 'react-native-easy-grid';
import getTheme from '../../../native-base-theme/components';
import commonColor from '../../../native-base-theme/variables/commonColor';

import ListTitle from "../../components/ListTitle";
import ExcerciseListItem from "../../components/ExcerciseListItem";
import Aside from "../../components/Aside";
import Api from "../../api/API";

import {
  Container, View, Header, Separator, Content, List, Right, Left, ListItem, Text, Body, Thumbnail, Button, Icon, Title, StyleProvider,
  H1, H2,} from 'native-base';


const styles = StyleSheet.create({
  h1: {
    ...sanFranciscoWeights.black,
    fontSize: 34,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h2: {
    ...sanFranciscoWeights.heavy,
    fontSize: 26,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h3: {
    ...sanFranciscoWeights.bold,
    fontSize: 20,
    letterSpacing: sanFranciscoSpacing(24),
  },
  subHeading: {
    
  },
  welcome: {
    fontSize: 20,
    color: "white",
    textAlign: "center"
  },
  instructions: {
    textAlign: "center",
    color: "white"
  }
});


export default class ExercisesPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      largeTileSize: 0,
      smallPageTitle: '',
      largePageTitle: 'Exercises',
      showAside: false,
      programs: [
        {
          title: 'Back Blaster',
          image: '../resources/images/abs.png',
          desc: 'test'
        },
        {
          title: 'Legs Day',
          image: '../resources/images/abs.png',
          desc: 'test'
        },
        {
          title: 'Chest Day',
          image: '../resources/images/abs.png',
          desc: 'test'
        }
      ],
      exercises: {
        absAndCore: [
          {
            title: 'Flat Bench Leg Raises',
            image: '../resources/images/abs.png',
            desc: 'This is an exercise for lower abdominal strengthening.',
            link: 'http://db.everkinetic.com/exercise/flat-bench-leg-raises/'
          },
          {
            title: 'Side Plank',
            image: '../resources/images/abs.png',
            desc: 'This is an exercise for core strengthening.',
            link: 'http://db.everkinetic.com/exercise/side-plank/'
          },
          {
            title: 'Bent Knee Hip Raise',
            image: '../resources/images/abs.png',
            desc: 'This is a good exercise core muscle development.',
            link: 'http://db.everkinetic.com/exercise/bent-knee-hip-raise/'
          },
          {
            title: 'Decline Crunch',
            image: '../resources/images/abs.png',
            desc: 'Using a decline crunch allows you keep your legs steady and isolate all of the abdominal muscles.',
            link: 'http://db.everkinetic.com/exercise/decline-crunch/'
          },
          {
            title: 'Cross Body Crunch',
            image: '../resources/images/abs.png',
            desc: 'This version of the crunch works both the upper and lower portion of the abs.',
            link: 'http://db.everkinetic.com/exercise/cross-body-crunch/'
          },
          {
            title: 'Crunches',
            image: '../resources/images/abs.png',
            desc: 'This is the most common abdominal exercise and possibly the most often improperly performed. Here is how to perform it correctly.',
            link: 'http://db.everkinetic.com/exercise/crunches/'
          },
          {
            title: 'Decline Oblique Crunch',
            image: '../resources/images/abs.png',
            desc: 'This version of a decline crunch isolates the oblique (side) muscles of the abs.',
            link: 'http://db.everkinetic.com/exercise/decline-oblique-crunch/'
          },
          {
            title: 'Stability Ball Abdominal Crunch',
            image: '../resources/images/abs.png',
            desc: 'This crunch uses a Stability Ball. The ball allows you a better range of movement because it adapts better to your spine.',
            link: 'http://db.everkinetic.com/exercise/stability-ball-abdominal-crunch/'
          },
          {
            title: 'Exercise Ball Pull In',
            image: '../resources/images/abs.png',
            desc: 'This exercise uses a ball to isolate and work the lower abdominal muscles.',
            link: 'http://db.everkinetic.com/exercise/exercise-ball-pull-in/'
          }
        ],
        arms: [
          {
            title: 'Tate Press',
            image: '../resources/images/abs.png',
            desc: 'This is an advanced triceps exercise which moves the muscle differently than other exercises.',
            link: 'http://db.everkinetic.com/exercise/tate-press/'
          },
          {
            title: 'Kneeling Triceps Extension with Cable',
            image: '../resources/images/abs.png',
            desc: 'Kneeling allows you to isolate your triceps more effectively.',
            link: 'http://db.everkinetic.com/exercise/kneeling-triceps-extension-with-cable-2/'
          },
          {
            title: 'Seated Overhead Triceps Extension with Barbell',
            image: '../resources/images/abs.png',
            desc: 'This exercise uses a barbell behind your neck to isolate the triceps effectively.',
            link: 'http://db.everkinetic.com/exercise/seated-overhead-triceps-extension-with-barbell/'
          },
          {
            title: 'Triceps Extensions using Machine',
            image: '../resources/images/abs.png',
            desc: 'This exercise uses a machine to insure proper range of motion. This is a good exercise for beginners and people rehabilitating from injury.',
            link: 'http://db.everkinetic.com/exercise/triceps-extensions-using-machine/'
          },
          {
            title: 'Bench Dips',
            image: '../resources/images/abs.png',
            desc: 'This exercise is one of the most basic and still one of the best for building the triceps (muscles on the back of the arm).',
            link: 'http://db.everkinetic.com/exercise/bench-dips/'
          },
          {
            title: 'Low Triceps Extension with Cable',
            image: '../resources/images/abs.png',
            desc: 'This exercise is done while lying on a seated row station.',
            link: 'http://db.everkinetic.com/exercise/low-triceps-extension-with-cable/'
          },
        ]
      }
    }
  }

  componentDidMount(){
  }

  show(cat){
    switch (cat) {
      case 'abs':
        this.setState({
          showAbs: !this.state.showAbs
        })
      break;
      case 'arms':
        this.setState({
          showArms: !this.state.showArms
        })
        break;
      case 'back':
        this.setState({
          showBack: !this.state.showBack
        })
        break;
      case 'chest':
        this.setState({
          showChest: !this.state.showChest
        })
        break;
      case 'shoulders':
        this.setState({
          showShoulders: !this.state.showShoulders
        })
        break;
      case 'legs':
        this.setState({
          showLegs: !this.state.showLegs
        })
        break;
      case 'cardio':
        this.setState({
          showCardio: !this.state.showCardio
        })
        break;
      case 'strength':
        this.setState({
          showStrength: !this.state.showStrength
        })
        break;
    }
    
    console.log(cat)
  }
  
  showAside(){
    this.setState({
      showAside: !this.state.showAside
    })
  }



  render() {
    return (
      <Container style={{ backgroundColor: '#FFF' }}>
        {/* ASIDE */}
          <Aside showAside={this.state.showAside}  />
        {/* END ASIDE */}
        
        <StyleProvider style={getTheme(commonColor)}>
          <Header >
            <Left/>
            <Body>
              <Title>{this.state.pageTitle}</Title>
            </Body>
            <Right />
          </Header>
        </StyleProvider>  
        
        <Content>
        
          <View style={{ paddingLeft: 5, paddingRight: 5, backgroundColor: '#fff' }}>
            <H1 style={ styles.h1 }> {this.state.largePageTitle}</H1>
          </View>

          <TouchableOpacity onPress={() => this.show('abs') } >
            <ListTitle text="Abs & Core" open={this.state.showAbs}  />
          </TouchableOpacity>

          <View style={{ height: this.state.showAbs ? 'auto' : 0, opacity: this.state.showAbs ? 1 : 0 }}>
            {this.state.exercises.absAndCore.map((data, key) => {
              return (
                <ExcerciseListItem data={data} key={key} showAside={this.showAside.bind(this)} />
              );
            })}
          </View>

          <TouchableOpacity onPress={() => this.show('arms')} >
            <ListTitle text="Arms" open={this.state.showArms}  />
          </TouchableOpacity>

          <View style={{ height: this.state.showArms ? 'auto' : 0, opacity: this.state.showArms ? 1 : 0 }}>
            {this.state.exercises.arms.map((data, key) => {
              return (
                <ExcerciseListItem data={data} key={key} />
              );
            })}
          </View>



          <TouchableOpacity onPress={() => this.show('back')} >
            <ListTitle text="Back" open={this.state.showBack}  />
          </TouchableOpacity>
          
          <TouchableOpacity onPress={() => this.show('chest')} >
            <ListTitle text="Chest" open={this.state.showChest}  />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.show('shoulders')} >
            <ListTitle text="Shoulders" open={this.state.showShoulders}  />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.show('legs')} >
            <ListTitle text="Legs" open={this.state.showLegs}  />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.show('cardio')} >
            <ListTitle text="Cardio" open={this.state.showCardio}  />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.show('strength')} >
            <ListTitle text="Strength" open={this.state.showStrength}  />
          </TouchableOpacity>

        </Content>
      </Container>
    )
  }
}  



AppRegistry.registerComponent('ExercisesPage', () => ExercisesPage);
