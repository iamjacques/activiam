import React, { Component } from 'react'
import { StyleSheet, AppRegistry, Image, Dimensions, ScrollView, TouchableHighlight, Alert } from 'react-native'
import ElevatedView from "fiber-react-native-elevated-view";
import { NavigationActions } from 'react-navigation';
import { systemWeights, iOSUIKit,sanFranciscoWeights, sanFranciscoSpacing, materialColors } from 'react-native-typography'
import { Col, Row, Grid } from 'react-native-easy-grid';
import getTheme from '../../../native-base-theme/components';
import commonColor from '../../../native-base-theme/variables/commonColor';

import {
  Container, View, Header, Separator, Content, List, Right, Left, ListItem, Text, Body, Thumbnail, Button, Icon, Title, StyleProvider,Fab,
  H1, H2,} from 'native-base';


const styles = StyleSheet.create({
  h1: {
    ...sanFranciscoWeights.black,
    fontSize: 34,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h2: {
    ...sanFranciscoWeights.heavy,
    fontSize: 26,
    letterSpacing: sanFranciscoSpacing(34),
  },
  h3: {
    ...sanFranciscoWeights.bold,
    fontSize: 20,
    letterSpacing: sanFranciscoSpacing(24),
  },
  subHeading: {
    
    },
    txt: {
        ...sanFranciscoWeights.ultraLight,
        fontSize: 14,
        letterSpacing: sanFranciscoSpacing(24),
  },
  welcome: {
    fontSize: 20,
    color: "white",
    textAlign: "center"
  },
  instructions: {
    textAlign: "center",
    color: "white"
  }
});


export default class ExercisePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      largeTileSize: 0,
      smallPageTitle: '',
        largePageTitle: '',
        active: 'true'
    }
  }

    goBack() {
        this.props.navigation.goBack();
    }  
    
  render() {
    return (
      <Container style={{ backgroundColor: '#FFF' }}>

            <Content>

            <View style={{ alignSelf: 'stretch', justifyContent: 'flex-end', height: 280, backgroundColor: '#EDF8FF', padding: 10 }}>
              <Text  style={ styles.h1 }> Dumbells  </Text> 
            </View>    

                
        




                <View style={{ paddingTop: 40, marginTop: 20, paddingRight: 10, paddingBottom: 10, paddingLeft: 10 }}> 
                    
                    <Text style={styles.text}> From open source to pro services, Ionic helps you build, deploy, test, and monitor apps easier than ever before. </Text>  

                    <Text style={styles.text}> From open source to pro services, Ionic helps you build, deploy, test, and monitor apps easier than ever before. </Text>  

                    <Text style={styles.text}> From open source to pro services, Ionic helps you build, deploy, test, and monitor apps easier than ever before. </Text>  
                    

          </View>

          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#5067FF' }}
            position="topRight"
            onPress={() => this.goBack()}>
            <Icon size={20} name="close" />
          </Fab>
        </Content>
      </Container>
    )
  }

}  



AppRegistry.registerComponent('ExercisePage', () => ExercisePage);
